import { defineConfig } from 'vite'
import { resolve } from 'path'
import path from 'path';
import react from '@vitejs/plugin-react'
import fs from 'fs/promises';
//import babel from 'vite-plugin-babel';

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    host: true,
    port: 8081,
    watch: {
      usePolling: true,
     },
  },
  esbuild: {
    loader: "jsx",
    include: /src\/.*\.jsx?$/,
    // loader: "tsx",
    // include: /src\/.*\.[tj]sx?$/,
    exclude: [],
  },
  optimizeDeps: {
    esbuildOptions: {
      plugins: [
        {
          name: "load-js-files-as-jsx",
          setup(build) {
            build.onLoad({ filter: /src\/.*\.js$/ }, async (args) => ({
              loader: 'jsx',
              contents: await fs.readFile(args.path, "utf8"),
            }));
          },
        },
      ],
    },
  },
  plugins: [react()],
})
